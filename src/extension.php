<?php

return [
    "name" => "ckfinder",
    "group" => "ckfinder",
    "path" => "lar-admin-extensions/ckfinder",
    "description" => "",
    "version" => "1.0.0",
    "icon" => "nut",
    "namespace" => "Lar\\Admin\\Ckfinder",
    "class" => "Ckfinder",
    "repo" => "admin-ext:lar-admin-extensions/ckfinder.git"
];