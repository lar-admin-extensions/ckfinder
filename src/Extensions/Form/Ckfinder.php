<?php

namespace Lar\Admin\Ckfinder\Extensions\Form;

use Lar\Admin\Components\AdminUi\AdminUiForm\AdminUiFormBase;

/**
 * Ckfinder Class
 * 
 * @package Lar\Admin\Ckfinder\Extensions\Form
 */
class Ckfinder extends AdminUiFormBase
{
    /**
     * Ckfinder constructor.
     * 
     * 
     * @param string $field
     * @param string $label
     * @throws \Exception
     */
    public function __construct(string $field, string $label = "") {
        
        parent::__construct($field, $label);
    }

}
