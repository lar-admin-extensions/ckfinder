<?php

namespace Lar\Admin\Ckfinder;

use Illuminate\Support\Arr;
use Lar\Admin\AdminServiceProvider;
use Lar\Admin\Middleware\Authenticate;

/**
 * CkfinderProvider Class
 * 
 * @package Lar\Admin\Ckfinder
 */
class CkfinderProvider extends AdminServiceProvider
{
    /**
     * Protected variable Commands
     * 
     * @var array
     */
    protected $commands = [
    
    ];

    /**
     * Protected variable Observers
     * 
     * @var array
     */
    protected $observers = [
    
    ];

    /**
     * Protected variable RouteMiddleware
     * 
     * @var array
     */
    protected $routeMiddleware = [
    
    ];

    /**
     * Protected variable MiddlewareGroups
     * 
     * @var array
     */
    protected $middlewareGroups = [
    
    ];

    /**
     * Public method boot
     * 
     * @return void
     */
    public function boot() {

        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        
        if (app()->runningInConsole()) {

            $this->publishes([
                __DIR__ . '/../resources/langs' => resource_path('lang/vendor/ckfinder'),
            ]);
        }

        if (is_array($cfg = config('ckfinder'))) {

            $cfg['authentication'] = Authenticate::class;

            $cfg['backends']['default']  = array_merge($cfg['backends']['default'], [
                'baseUrl' => \Storage::disk(config('admin.upload.disk'))->url(""),
                'root' => config('filesystems.disks.admin.root'),

            ]);

            config(['ckfinder' => $cfg]);
        }
    }

    /**
     * Public method register
     * 
     * @return void
     */
    public function register() {
        
        \Admin::registerExtension('lar-admin-extensions/ckfinder');
        $this->registerRouteMiddleware();
        $this->commands($this->commands);
        $this->registerObservers();
        $this->loadMigrationsFrom(__DIR__ . '/../resources/migrations');
        Authenticate::addBootstrap(__DIR__ . '/bootstrap.php');
    }

}
