<?php

namespace Lar\Admin\Ckfinder;

use Lar\Admin\Commands\Update;
use Lar\Layout\Commands\HeaderDumpUpdate;
use Lar\Admin\Commands\ExtendInstall;
use Lar\Admin\Commands\ExtendRemove;

/**
 * Ckfinder Class
 * 
 * @package Lar\Admin\Ckfinder
 */
class Ckfinder
{
    /**
     * Static method update
     * 
     * @return void
     */
    static function update(Update $update) {
        
        $update->info('Extension [Ckfinder] updated!');
    }

    /**
     * Static method dump
     * 
     * @return void
     */
    static function dump(HeaderDumpUpdate $dump) {
        
        $dump->info('Extension [Ckfinder] dumped!');
    }

    /**
     * Static method install
     * 
     * @return void
     */
    static function install(ExtendInstall $installer) {

        $installer->call("ckfinder:download");

        $installer->call("vendor:publish", ["--tag" => "ckfinder"]);

        $installer->info('Extension [Ckfinder] installed!');
    }

    /**
     * Static method uninstall
     * 
     * @return void
     */
    static function uninstall(ExtendRemove $uninstaller) {
        
        $uninstaller->info('Extension [Ckfinder] run uninstall...');
    }

}
