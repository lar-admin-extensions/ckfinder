<?php

namespace Lar\Admin\Ckfinder;

use Illuminate\Support\Facades\Facade;

/**
 * CkfinderFacade Class
 * 
 * @package Lar\Admin\Ckfinder
 */
class CkfinderFacade extends Facade
{
    /**
     * Protected Static method getFacadeAccessor
     * 
     * @return void
     */
    protected static function getFacadeAccessor() {
        return Ckfinder::class;
    }

}
